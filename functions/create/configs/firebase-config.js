
// Stored in Netlify
const { API_KEY, APP_ID } = process.env

const firebaseConfig = {
  apiKey: API_KEY,
  authDomain: 'avatar-generator-2919c.firebaseapp.com',
  projectId: 'avatar-generator-2919c',
  storageBucket: 'avatar-generator-2919c.appspot.com',
  messagingSenderId: '705790785475',
  appId: APP_ID,
  measurementId: 'G-MKB53JWTLL'
}

module.exports = firebaseConfig
