const avatarsConfig = {
  characterFolders: { // represents all folders and variations in Firebase
    1: {
      name: '1',
      faceAmount: 9 // since we have different amount of face files in every folder
    },
    2: {
      name: '10',
      faceAmount: 9
    },
    3: {
      name: '11',
      faceAmount: 9
    },
    4: {
      name: '2',
      faceAmount: 9
    },
    5: {
      name: '3',
      faceAmount: 9
    },
    6: {
      name: '4',
      faceAmount: 9
    },
    7: {
      name: '5',
      faceAmount: 3
    },
    8: {
      name: '6',
      faceAmount: 3
    },
    9: {
      name: '7',
      faceAmount: 3
    },
    10: {
      name: '8',
      faceAmount: 9
    },
    11: {
      name: '9',
      faceAmount: 9
    }
  },
  avatarParts: {
    1: {
      id: 1,
      partName: 'rightArm',
      filename: 'Right_Arm.png',
      order: 1,
      position: {
        top: 390,
        left: 165
      }
    },
    2: {
      id: 2,
      partName: 'rightHand',
      filename: 'Right_Hand.png',
      order: 2,
      position: {
        top: 425,
        left: 135
      }
    },
    3: {
      id: 3,
      partName: 'leftArm',
      filename: 'Left_Arm.png',
      order: 3,
      position: {
        top: 390,
        left: 330
      }
    },
    4: {
      id: 4,
      partName: 'leftHand',
      filename: 'Left_Hand.png',
      order: 4,
      position: {
        top: 425,
        left: 360
      }
    },
    5: {
      id: 5,
      partName: 'rightLeg',
      filename: 'Right_Leg.png',
      order: 5,
      position: {
        top: 525,
        left: 200
      }
    },
    6: {
      id: 6,
      partName: 'leftLeg',
      filename: 'Left_Leg.png',
      order: 6,
      position: {
        top: 525,
        left: 305
      }
    },
    7: {
      id: 7,
      partName: 'body',
      filename: 'Body.png',
      order: 7,
      position: {
        top: 310,
        left: 150
      }
    },
    8: {
      id: 8,
      partName: 'head',
      filename: 'Head.png',
      order: 8,
      position: {
        top: 25,
        left: 100
      }
    },
    9: {
      id: 9,
      partName: 'face',
      filePrefix: 'Face_',
      order: 9,
      position: {
        top: 220,
        left: 150
      }
    }
  }
}
module.exports = avatarsConfig
