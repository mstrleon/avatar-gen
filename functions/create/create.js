// Avatar Generating - using Firabase as a storage for variants and final result
// (c) Leon Vasilyev

// Simple CORS
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Methods': 'POST, PUT, DELETE'
}

// Firebase database and storage
const firebase = require('firebase/app')
const storage = require('./lib/firebase-storage.js')

// fake browser xhr for firebase-storage on nodejs
const XMLHttpRequest = require('xhr2')
const xhr = new XMLHttpRequest()

// site-specific config
const firebaseConfig = require('./configs/firebase-config')
const { avatarParts } = require('./configs/avatar-config.js')

const helpers = require('./helpers.js')

const axios = require('axios')
const sharp = require('sharp')

const handler = async (event, context, callback) => {
  // CORS proccessing
  if (event.httpMethod === 'OPTIONS') {
    return callback(null, { statusCode: 200, headers, body: 'This was a preflight call!' })
  }

  // VALIDATE REQUEST START
  const errors = []
  if (!event.body) {
    errors.push('BODY should exist')
  }

  // FIREBASE INIT
  const app = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app()
  const rootRef = firebase.storage().ref()
  const appRef = rootRef.child('templates')
  let folderRef

  let URLs = {}
  const refs = []

  try {
    switch (event.httpMethod) {
      case 'GET':
        const dna = helpers.generateDNA()
        console.log('DNA generated', dna)
        for (const key in dna) {
          try {
            folderRef = appRef.child(dna[key].folder)
            refs.push(folderRef.child(dna[key].filename))
          } catch (e) {
            console.error('problem allocating file  ' + dna[key].filename, dna[key], folderRef, e)
          }
        }

        let resp = []

        try {
          resp = await Promise.all(
            Object.values(refs).map(ref => ref.getDownloadURL())
          )
        } catch (e) { // TODO then trying default pic
          console.error('No picture Found')
          console.error('getDownloadURL Error: ', e)
        }

        URLs = Object.assign({}, resp)

        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'application/json'
          },
          // body: JSON.stringify(URLs)
          body: JSON.stringify(URLs)
        }

      case 'POST': // Here we process received urls and create avatar

        URLs = JSON.parse(event.body)

        const buffers = await Promise.all(
          Object.values(URLs).map(url => {
            return axios.get(url, { responseType: 'arraybuffer' })
          })
        )

        console.log('START MERGING....')

        const { avatarDefault } = require('./assets/default.js')

        // console.log(avatarDefault)
        let avatar = avatarDefault

        let merged

        try {
          merged = await sharp({ create: { width: 700, height: 700, channels: 4, background: { r: 0, g: 0, b: 0, alpha: 0 } } })
            .composite([{
              input: buffers[0].data,
              top: avatarParts[1].position.top,
              left: avatarParts[1].position.left
            }])
            .png()
            .toBuffer()

          for (let i = 1; i < buffers.length; i++) {
            try {
              merged = await sharp(merged)
                .composite([{
                  input: buffers[i].data,
                  top: avatarParts[i + 1].position.top,
                  left: avatarParts[i + 1].position.left
                }])
                .png()
                .toBuffer()
            } catch (e) {
              console.error('Error on merging i, buffers[i]', i, buffers[i], e)
            }
          }
        } catch (e) {
          console.log('Error with merging parts', e)
        }

        merged = await sharp(merged).resize({ width: 300 }).toBuffer()

        avatar = Buffer.from(merged, 'binary').toString('base64')

        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'data:image/png;base64'
          },
          body: avatar
        }

      default:
        return {
          statusCode: 500,
          body:
            JSON.stringify('only GET or POST')
        }
    }
  } catch (error) {
    return {
      statusCode: 500,
      body: error.toString()
    }
  }
}

module.exports = { handler }
