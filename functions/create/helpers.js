const { characterFolders, avatarParts } = require('./configs/avatar-config')

const helpers = {
  generateDNA () {
    const dna = avatarParts
    console.log('starting generating Dna')

    // here generate hands and arms and legs, only just once, for both L and R
    const variantsAmount = Object.keys(characterFolders).length
    const legVariant = getRandomInt(variantsAmount)
    const handVariant = getRandomInt(variantsAmount)
    const armVariant = getRandomInt(variantsAmount)

    for (const key in avatarParts) {
      dna[key] = avatarParts[key]
      switch (dna[key].partName) {
        case 'rightHand':
        case 'leftHand':
          dna[key].selectedVariant = handVariant
          break
        case 'rightLeg':
        case 'leftLeg':
          dna[key].selectedVariant = legVariant
          break
        case 'rightArm':
        case 'leftArm':
          dna[key].selectedVariant = legVariant
          break
        default:
          dna[key].selectedVariant = getRandomInt(variantsAmount)
      }
      dna[key].folder = dna[key].selectedVariant.toString()
      if (dna[key].partName === 'face') { // gotta change filename for face, since we have different
        // face expressions in every folder
        const arrFolders = Object.keys(characterFolders).map((key) => characterFolders[+key])
        const faceAmount = arrFolders.find(folder => folder.name === dna[key].folder).faceAmount
        console.log('faceAmount', faceAmount)
        const fileVariant = getRandomInt(faceAmount, 7)
        const fileSuffix = fileVariant > 10
          ? fileVariant.toString()
          : '0' + fileVariant.toString()
        dna[key].filename = dna[key].filePrefix + fileSuffix + '.png'
        console.log('face Filename:', dna[key].filename)
      }
    }
    return dna
  }
}

function getRandomInt (max, excludeNum) {
  if (!excludeNum) { return (Math.floor(Math.random() * max) + 1) }
  let rand = -1
  do {
    rand = (Math.floor(Math.random() * max) + 1)
  } while (rand === excludeNum)
  return rand
}

module.exports = helpers
